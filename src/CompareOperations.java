import java.util.List;

public class CompareOperations {
    public static void compareCreating(List<Integer> linked, List<Integer> array) {
        long timeBeforeA = System.currentTimeMillis();
        ListTest.createList(linked);
        long timeAfterA = System.currentTimeMillis();
        long resultA = timeAfterA - timeBeforeA;

        long timeBeforeB = System.currentTimeMillis();
        ListTest.createList(array);
        long timeAfterB = System.currentTimeMillis();
        long resultB = timeAfterB - timeBeforeB;

        System.out.printf("Время выполнения создания:" +
                "\n linked листа: %d миллисекунд%n" +
                " array листа: %d миллисекунд%n", resultA, resultB);
    }


    public static void compareAdding(List<Integer> linked, List<Integer> array, int i) {
        long timeBeforeA = System.currentTimeMillis();
        ListTest.addElement(linked, i);
        long timeAfterA = System.currentTimeMillis();
        long resultA = timeAfterA - timeBeforeA;

        long timeBeforeB = System.currentTimeMillis();
        ListTest.addElement(array, i);
        long timeAfterB = System.currentTimeMillis();
        long resultB = timeAfterB - timeBeforeB;

        System.out.printf("Время выполнения добавления (add):" +
                "\n для linked листа: %d миллисекунд%n" +
                " для array листа: %d миллисекунд%n", resultA, resultB);
    }

    public static void comparePrinting(List<Integer> linked, List<Integer> array) {
        long timeBeforeA = System.currentTimeMillis();
        ListTest.printElements(linked);
        long timeAfterA = System.currentTimeMillis();
        long resultA = timeAfterA - timeBeforeA;

        long timeBeforeB = System.currentTimeMillis();
        ListTest.printElements(array);
        long timeAfterB = System.currentTimeMillis();
        long resultB = timeAfterB - timeBeforeB;

        System.out.printf("Время выполнения вывода (get):" +
                "\n для linked листа: %d миллисекунд%n" +
                " для array листа: %d миллисекунд%n", resultA, resultB);
    }

    public static void compareDeleting(List<Integer> linked, List<Integer> array) {
        long timeBeforeA = System.currentTimeMillis();
        ListTest.deleteElements(linked);
        long timeAfterA = System.currentTimeMillis();
        long resultA = timeAfterA - timeBeforeA;

        long timeBeforeB = System.currentTimeMillis();
        ListTest.deleteElements(array);
        long timeAfterB = System.currentTimeMillis();
        long resultB = timeAfterB - timeBeforeB;

        System.out.printf("Время выполнения удаления (remove):" +
                "\n для linked листа: %d миллисекунд%n" +
                " для array листа: %d миллисекунд%n", resultA, resultB);
    }

    public static void compareEditing(List<Integer> linked, List<Integer> array) {
        long timeBeforeA = System.currentTimeMillis();
        ListTest.editElements(linked);
        long timeAfterA = System.currentTimeMillis();
        long resultA = timeAfterA - timeBeforeA;

        long timeBeforeB = System.currentTimeMillis();
        ListTest.editElements(array);
        long timeAfterB = System.currentTimeMillis();
        long resultB = timeAfterB - timeBeforeB;

        System.out.printf("Время выполнения редактирования (set)" +
                "\n для linked листа: %d миллисекунд%n" +
                " для array листа: %d миллисекунд%n", resultA, resultB);
    }
}
