import java.util.List;
import java.util.Random;

public class ListTest {

    static void createList(List<Integer> g) {
        for (int i = 0; i < 1_000_000; i++) {
            g.add(i);
        }
    }

    //добавление элементов в начало списка
    public static void addElement(List<Integer> g, int a) {
        for (int i = 0; i < a; i++) {
            g.add(0, a);
        }
    }

    //печатает на экран 100 элементов из середины (например 500_000 - 500_100)
    public static void printElements(List<Integer> g) {
        for (int i = 500_000; i <= 500_100; i++) {
            System.out.println("Подсписок, элемент " + i + " - " + g.get(i));
        }
    }

    //удаляет 1000 первых элементов из списка
    public static void deleteElements(List<Integer> g) {
        g.removeIf(next -> next < 1000);
    }

    //устанавливает новые значения для 100 элементов из середины
    public static void editElements(List<Integer> g) {
        Random random = new Random();
        for (int i = 500_000; i <= 500_100; i++) {
            g.set(i, random.nextInt());
        }
    }


}

