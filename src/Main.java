import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> sunAge = new LinkedList<>();
        List<Integer> sunAge2 = new ArrayList<>();

        CompareOperations.compareCreating(sunAge, sunAge2);
        CompareOperations.comparePrinting(sunAge, sunAge2);
        CompareOperations.compareAdding(sunAge, sunAge2, 1000);
        CompareOperations.compareDeleting(sunAge, sunAge2);
        CompareOperations.compareEditing(sunAge, sunAge2);
    }

    /*
    По сравнению видно, что вывод и редактирование (get, set) на больших значениях отличается в сотни раз по скорости.
    У linked листа заметно дольше, чем у array (получалась разница примерно в 90-150 миллисекунд).

    С другой стороны, у array листа дольше добавление (add). Так же медленнее удаление (примерно на 4 миллисекунды).

    Насколько я помню, это связано с тем, как устроена индексация и связи внутри листов:
     - у array при добавлении и удалении приходится каждый раз перезаписывать целиком объект и как бы "сдвигать" их внутри листа.
     Зато при поиске можно напрямую обратиться к индексу и потому изменение и чтение происходит быстрее
     - у linked при добавлении и удалении переписывается только цепочка связей, сами объекты остаются нетронутыми.
     Но именно это мешает выводу и редактированию объектов, приходится обращаться по всей цепочке объектов, пока не будет
     найден нужный для удаления\редактирования.


     */

}
